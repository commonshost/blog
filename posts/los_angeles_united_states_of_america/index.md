---
date: 2018-09-07
title: Los Angeles, United States of America 🇺🇸
published: true
description: First Commons Host CDN edge server deployment in the Western Hemisphere.
tags: #cdn #edge #pop
---

## Go West, young CDN.

America, birth place of the Internet, joins the [Commons Host CDN](https://commons.host) with an edge server deployed in Los Angeles, California. As the largest city in the most populous state, this is a great location for low latency static site hosting.

![Scenic view of Los Angeles daytime skyline](./los_angeles_skyline.jpg)

*Photo: Los Angeles, by [Jelle Bleyenbergh (Flickr)](https://www.flickr.com/photos/jellebleyenbergh/43603169272/in/photolist-26qHNY1-256qawg-29WjWjz-2aHYmME-26HYWYR-23BGjAu-27dxzmL-26x4nJ7-LGutDh-E3VZck-23hh2VQ-MMhtm9-23SnPka-23BGkzy-E1tby4-23d3mx7-25UaXmY-23v3E35-269p1ZE-29v9uRa-F4HBrC-24A3bhu-227PisY-23d3n4h-25G1a8U-29r4D7y-KckgH2-22gUxKT-26WxvgM-M4RoxM/)*

## Millicast WebRTC CDN

This edge server is sponsored by WebRTC specialists [CoSMo Software](http://www.cosmosoftware.io). Its polymath founder [Dr Alex](https://twitter.com/agouaillard) is a [WebRTC industry insider](http://webrtcbydralex.com) and long time supporter of the Commons Host project. WebRTC enables open-standards based media streaming in browsers and devices. *(More exciting news to follow!)*

Hosting of the server in LA is provided by [Millicast](https://millicast.com), a WebRTC-powered live streaming CDN offering incredible sub-500 milliseconds latency for large scale broadcasts. Millicast is a collaboration between CoSMo and [Xirsys](https://xirsys.com) as provider of TURN cloud services.

![Cofounder of Millicast with traditional Southern California greeting hand gesture](./richard_blakely_selfie.jpg)

*Photo: Selfie by [Richard Blakely](https://twitter.com/RichardBlakely), [CEO of Xirsys](https://www.linkedin.com/in/richard-blakely-960796/) and cofounder of Millicast, with Commons Host µPoP server. 🤙🏻 for scale.*

## Together we serve, divided we scale.

This deployment is a small start, literally and figuratively, into the American CDN market. Obviously, many established American tech companies already provide competitive hosting and edge delivery services.

But what Commons Host offers is seamless extensibility into any location around the world. This offers better performance, cost savings, and unique operational capabilities.

The ability to extend the Commons Host CDN by deploying your own edge servers on-premises is made affordable and effortless thanks to open source software and hardware.

Join the Commons, deploy today!
