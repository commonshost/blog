---
date: 2018-05-06
title: Singapore 🇸🇬
published: true
description: Second CDN PoP edge server
tags: cdn, pop, edge
---

The second [Commons Host](https://commons.host) point of presence (PoP) launched this weekend. This is an important milestone in realising the *Network* aspect of a *Content Delivery Network* (CDN).

This new PoP is the first live deployment of a [Little Lamb Mk I](https://dev.to/commonshost/little-lamb-mk-i-5gf3) server.

![Two Little Lambs](./lambs.jpg)
<center>Photo: [Ben (seabirdnz)](https://www.flickr.com/photos/seabirdnz/15509455436)</center>

The second Commons Host PoP, like the first, is hosted on a residential fibre Internet connection in Singapore. Both servers are part of the same *pool*. Traffic is balanced within a pool according to the capacity of each server. For upcoming deployments in other locations new pools can be created per country, city, ISP, and more.

Automatic failover provides improved availability of the overall static hosting & CDN service. All PoPs are continuously monitored and automatically taken out of the pool while they are unavailable.
