---
date: 2018-05-15
title: Kuala Lumpur, Malaysia 🇲🇾
published: true
description: Commons Host Little Lamb Mk I server deployment in Kuala Lumpur, Malaysia
tags: cdn, edge, pop
---

This weekend a Commons Host Little Lamb Mk I server was deployed in Kuala Lumpur (KL), to better serve Internet users across Malaysia. This is the third Commons Host point of presence (PoP) overall, and the first located outside my home town of Singapore.

![Telekom Tower](./telekom_tower.jpg)
Photo: Telekom Building in Kuala Lumpur, Malaysia (attributed to [ReaperSpectre on Wikimapia](http://wikimapia.org/28233/Menara-TM#/photo/1742434))

Like most Asian cities, Kuala Lumpur offers several fibre ISPs serving residential and SME business customers at quite reasonable prices. Deploying low-cost CDN edge servers on commodity fibre connections is a novel alternative to expensive, or non-existent, enterprise-grade datacentres in many parts of the world.

The KL deployment started at 7 AM with a 4 hour motorcycle ride from Singapore to hand-deliver the tiny server. This was an unintentional vibration test for Little Lamb. Fortunately she is entirely solid state hardware and passed with flying colours.

![Seb passed out on couch with supportive friends](./resting_friends.jpg)
Photo: Seb passed out on couch immediately following successful CDN edge server deployment. Thanks to wonderful friends for the support.

Automation of the deployment process [using Ansible](https://gitlab.com/commonshost/ansible/issues) is a work in progress. It took a few hours to manually set up the server. The GeoDNS configuration, using [Constellix](https://constellix.com), now routes all traffic from visitors in Malaysia to the Kuala Lumpur PoP, with the two pre-existing Singapore PoPs as failover. Late Sunday evening the job was finished. Thanks to the company of good friends who morally and physically supported me.

![Photo: Seb presenting a brown bag lunch session at NEXT Academy Kuala Lumpur](./speaking_next_academy.jpg)

On Monday, at [NEXT Academy Kuala Lumpur](https://www.nextacademy.com), I gave a two minute *brown bag lunch* talk followed by audience Q&A. Lots of great questions from sharp students, mentors, and instructors alike. Both about technical aspects as well as the sustainable business model of a Free and Open Source Software (FOSS) project.

May the lessons learned during this deployment help roll out many more deployments around the world.
