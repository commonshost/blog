---
date: 2018-04-29
title: Little Lamb Mk I
published: true
description: The smallest CDN edge server to build the largest CDN network.
tags: hardware, cdn
---

This little CDN edge server is open hardware, runs a completely FOSS stack, and is very affordable. These attributes make it a key enabler for many deploying CDN Points-of-Presence (PoPs). Especially in areas without traditional datacentres. The goal is to get closer, in terms of latency, to the entire global population than any other CDN.

![Little Lamb Mk I](./server.jpg)

The Mk I hardware consists of a [HardKernel Odroid HC1](http://www.hardkernel.com/main/products/prdt_info.php?g_code=G150229074080) with a 250 GB Samsung 850 EVO SSD. Its power-efficient 8-core ARM CPU serves [well over 800 Mbps in benchmarks](https://twitter.com/sebdeckers/status/987151171251945473). Since the 6 Gbps SATA drive is bottlenecked by the 1 Gbps network interface, I am looking at optimising for cost by using a cheaper SSD that maintains similar IOPS.

The Mk I model is [now available for sale](https://carousell.com/p/commons-host-cdn-pop-edge-server-166149743/). Automated deployment and dashboard integration is coming soon.

Pricing is currently calculated as follows:

1. Bill of Materials (BOM)
1. +10% source shipping & handling (S&H)
1. +7% GST ([gahmen](https://en.wiktionary.org/wiki/gahmen))
1. +33% setup fee

So, by building your own server you could save 25%. To put that into perspective, a rule of thumb for hardware startups is to charge 200% markup to stay profitable. So, clearly, at a fraction of the usual margin, I am not starting a hardware business. Commons Host is a network business. By keeping the hardware margin low, and the software free as in gratis, more servers will be deployed. [Metcalfe's Law](https://en.wikipedia.org/wiki/Metcalfe's_law) predicts that the value of the network should increase exponentially.
