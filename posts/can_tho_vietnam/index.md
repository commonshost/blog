---
date: 2018-07-20
title: Cần Thơ, Vietnam 🇻🇳
published: true
description: Expanding the CDN with a new PoP in a massively populated region of Vietnam.
tags: cdn, edge, pop
---

The [Commons Host](https://commons.host) CDN expands with a point-of-presense (PoP) in Cần Thơ, Vietnam. This is the largest city in the Mekong River Delta region of 17 million people. Just 150 km away is Ho Chi Minh City (HCMC), a metropolitan area of another 13 million people.

This massive population is drastically under-served by conventional CDNs, many of whom lack local infrastructure or peering agreements with incumbent ISPs.

![Can Tho panorama](./hotel_xoai_panorama.jpg)
Photo: Panorama of Cần Thơ by [Chung Wei Tat](https://twitter.com/cweitat) at [Hotel XOAI](http://hotelxoai.com) the [FOSSASIA](https://fossasia.org) HQ

## A Different Market

There is limited bandwidth going in-and-out of the country. While local datacentres do exist, they typically come at a hefty premium over similar infrastructure in more saturated markets like the United States. Commons Host is still just a tiny, free & open source service, so expensive datacentre co-location is out of the question.

That is where Commons Host takes a novel approach using open hardware and commodity fibre internet connections.

## Open Hardware

The [Little Lamb Mk I](https://dev.to/commonshost/little-lamb-mk-i-5gf3) micro-servers, based on the [Odroid HC-1](http://magazine.odroid.com) platform and running open source [Node.js](https://nodejs.org/en/) software, are ideal for hyper-local deployments.

Critical financial assistance came from [Tien Nguyen](https://twitter.com/viettienn), [CTO](https://www.linkedin.com/in/viettienn/) at [Wego](https://www.wego.com), who purchased the server to support the CDN effort.

## Commodity Fibre

Consumer fibre internet access is widely available in most Asian cities, including Vietnam. So the Commons Host PoP is connected via commodity broadband. The tiny power consumption means that a basic UPS has been enough to deal with intermittent electricity grid outages, resulting in 100% uptime over the first 3+ weeks of operation.

The physical hosting is generously provided by [Hotel XOAI](http://hotelxoai.com), also known as the [FOSSASIA](https://fossasia.org) HQ. Several kind members from the FOSSASIA community stepped up to help. Firstly, [Chung Wei Tat](https://twitter.com/cweitat) handled the logistics, i.e. packing the small server in his carry-on luggage. Then, [Daniel J Blueman](https://www.linkedin.com/in/danielblueman/) remotely handled testing and configuration of the network. *Protip: Stay at Hotel XOAI for what is probably the best hotel wifi in Vietnam.*

## Changing the Game

Being able to affordably deploy many PoPs, across ISPs and cities, is ideal for CDNs in these markets. And for the local ISPs having a PoP on their network keeps traffic local which helps them avoid expensive transit fees. As the Commons Host CDN service gains traction, more and/or larger PoPs can be easily deployed to scale capacity.

Work has started to translate all the Commons Host website content and developer documentation. Part of deploying PoPs in new regions is helping local developers learn how to adopt it to better serve their users.

Many thanks to [Tien Nguyen](https://twitter.com/viettienn) and the [FOSSASIA](https://fossasia.org) community. Team work makes the dream work.
