---
date: 2018-10-21
title: Secure your DNS with DoH in 1 line
published: false
description: Think big. Start small. Move fast.
tags: showdev, node, devops, opensource
---

## Hello RFC8484, DoH!

It's official: [RFC8484 *"DNS Queries over HTTPS (DoH)"*](https://www.rfc-editor.org/info/rfc8484) has been published by the IETF RFC Editor! This moves the document from *Draft* status to the *Proposed Standard* stage. This penultimate stage in the IETF Standards Track process means the document is considered **stable and well-reviewed**.

A total of [18 DoH draft documents](https://datatracker.ietf.org/doc/rfc8484/) were published in just over a year. Blistering speed by IETF norms. For comparison, [RFC7540, the HTTP/2 specification](https://datatracker.ietf.org/doc/rfc7540/), has been a Proposed Standard since May 2015. It took more than 3 years to iron out the finer details of HTTP/2 in over 19 drafts.

## Born of Snowden

The urgency driving DoH is the need to strengthen DNS against censorship, tampering, and large scale monitoring. Concerns around DNS privacy and censorship have existed for years around the world. The NSA revelations by Edward Snowden brought these problems closer to home for many in the Internet standards community.

DNS traffic was always clear text. This makes it easy for ISPs to record and modify all queries a customer makes. Redirecting, dropping, or otherwise tampering with DNS queries is a widely used form of censorship, whether it is to enforce copyright, political control, or other reasons.

To those aiming to control DNS, lack of encryption is a feature. To DoH, it is a bug.

## DNSSEC, DoT & DoH

Other standards exist to protect DNS traffic. First there is DNSSEC to cryptographically sign DNS records. This prevents tampering with record data. But lookups and responses are still transferred in plaintext.

To protect the privacy of DNS traffic, full encryption is required. One proposal is to 

## No More Amplification Attacks

Using simple UDP messages, instead of TCP connections, exposed the protocol to address spoofing. This is a major factor in DDoS attacks. Attackers send small DNS queries to public DNS resolvers with faked source address set to the IP of the intended victim. DNS responses, several times larger than the queries, are sent by the public DNS resolver to the victim's address, overwhelming their Internet connection with traffic.

Various standards address these issue.

- DNSSEC allows DNS records to by cryptographically signed. Any tampering is prevented.

```
npx dohnut --verbose --port 5300 --doh https://cloudflare-dns.com/dns-query | npx pino-colada
```

```
npx: installed 29 in 2.884shMetadata: sill resolveWithNewModule pino@5.8.0 checking installable status
npx: installed 15 in 3.93s
```

```
12:17:53 ✨ dohnut listening on 0.0.0.0:5300
12:18:05 ✨ query received
12:18:05 ✨ reply sent
```

```
dig @localhost -p 5300 www.google.com

; <<>> DiG 9.10.6 <<>> @localhost -p 5300 www.google.com
; (2 servers found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 50617
;; flags: qr rd ra; QUERY: 1, ANSWER: 6, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1452
;; QUESTION SECTION:
;www.google.com.			IN	A

;; ANSWER SECTION:
www.google.com.		255	IN	A	74.125.130.106
www.google.com.		255	IN	A	74.125.130.147
www.google.com.		255	IN	A	74.125.130.99
www.google.com.		255	IN	A	74.125.130.103
www.google.com.		255	IN	A	74.125.130.104
www.google.com.		255	IN	A	74.125.130.105

;; Query time: 4 msec
;; SERVER: 127.0.0.1#5300(127.0.0.1)
;; WHEN: Sun Oct 21 12:14:38 +08 2018
;; MSG SIZE  rcvd: 139
```
