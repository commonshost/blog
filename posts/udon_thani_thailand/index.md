---
date: 2018-10-11
title: Udon Thani, Thailand 🇹🇭
published: true
description: Welcome to the land of smiles.
tags: #cdn #edge #pop
---

[Commons Host](https://commons.host) expands in Thailand with a new edge location to serve low-latency web and DNS traffic.

Thailand is the second largest economy in South East Asia. It has a population of over 68 million people, slightly more than France or the United Kingdom.

The new edge server is located in the north-eastern city of Udon Thani. Routing is conservatively configured to receive only traffic from within Thailand.

The server hardware and hosting are sponsored by [Dr Alex](https://twitter.com/agouaillard), founder of Singapore based WebRTC specialists [CoSMo Software](http://www.cosmosoftware.io/). He is a long-time supporter of Commons Host who previously sponsored the [Los Angeles edge server](https://dev.to/commonshost/los-angeles-united-states-of-america--ok7) as well as my first in-person attendance to an [IETF conference](https://www.ietf.org/how/meetings/100/). Huge thanks for his support and mentorship.

![Street view in Udon Thani, Thailand](./udon-thani-street.jpg)

*Photo: Street view Udon Thani, Thailand (Source: [Insights Unspoken](https://www.flickr.com/photos/insightsunspoken/30698563151))*

## Regional Peering Surprises

From Udon Thani the Laotian capital Vientiane is geographically closer than Thai capital Bangkok, respectively 60 km and 460 km. However network latency tells a different story.

| Route | Distance | Round Trip Time |
|-|-|-|
| Udon Thani to Bangkok | 460 km | 12 ms |
| Udon Thani to Vientiane | 60 km | 25 ms |

![Map of traffic flow from Udon Thani to Vientiane via Bangkok](./uth-bkk-vte.jpg)

*Map: Geographical distance (green) vs Network distance (red)*

As another example let's look at the edge server in Udon Thani and two different ISPs in Singapore. One has direct peering, at an IX in Singapore, while the other does not. Without peering they must fall back to a carrier in Palo Alto, California; literally an ocean away.

Before this new edge server was deployed, traffic from users on this ISP in Thailand to the Singapore edge servers would sometimes travel halfway around the world.

| ISP | ISP | Round Trip Time | Internet Exchange |
|-|-|-|-|
| Commons Host @ Udon Thani (Thailand) | [StarHub](https://www.peeringdb.com/org/1064) (Singapore) | 42 ms | Singapore |
| Commons Host @ Udon Thani (Thailand) | [MyRepublic](https://www.peeringdb.com/net/4855) (Singapore) | 218 ms | California, USA |

![Map of traffic flow from Udon Thani to Singapore via California](./uth-sjc-sin.jpg)

*Map: Network distance from Udon Thani to Singapore with peering (1,800 km in green) or via long-haul transit (26,000 km in orange)*

## Bypassing Internet Exchanges

Network providers physically interconnect at Internet Exchanges (IX). In this case the IX is located in Bangkok, so all traffic between Udon Thani and Vientiane does an additional ~900 km round trip.

The problem is even worse between smaller ISPs. Since there may not be much traffic flowing between them, direct peering agreements are sometimes not in place. In those cases traffic can only be exchanged via a mutual peering partner.

Simply by deploying more servers in many locations worldwide we can side-step the lack of peering between ISPs. Deploying smaller servers in larger numbers is the brute force way to achieve ultra-low latency. It eliminates expensive & slow transit traffic.

Being able to deploy small-scale edge servers is a key advantage of the Commons Host CDN model.

| CDN Provider | Nearest edge to Udon Thani | Round Trip Time |
|-|-|-|
| Commons Host | Udon Thani | <1 ms |
| Cloudflare | Bangkok | ~12 ms |
| AWS Cloudfront | Singapore | >40 ms |

The ability to achieve lower latency is inversely proportional to the minimum size of an edge server deployment. Micro-servers < server racks < data centres.

## Consumer ISP Port Blocking, Not So Bad After All?

When Commons Host first started one of the fears was that ISPs would never allow it. Fortunately that has been mostly overcome by simply purchasing a static IP and having a friendly chat with the ISP support desk.

To deploy the Udon Thani edge server on a consumer fibre connection required obtaining a static IP address. In theory a dynamic IP address would suffice, but in reality those tend to be private addresses (10.x.x.x or 192.168.x.x) behind an ISP-wide Network Address Translator (NAT) and therefore inaccessible to the rest of the Internet.

Once the static, public IP address was activated, the next obstacle was a mysterious blocking of ports 80 and 443, for HTTP and HTTPS respectively. A simple call to the ISP by Dr Alex resolved the issue. Turns out all we had to do was ask and they happily opened the ports, allowing the edge server to go live in Thailand.

People are mostly good; this is a victory of the commons.
