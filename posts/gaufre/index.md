---
date: 2020-03-10
title: Gaufre, a Gopher browser in your Web browser
published: false
tags: gopher, showdev
---

Introducing **Gaufre**, a Gopher browser in your Web browser.

Launch the Gaufre web client:

https://gopher.commons.host

{% twitter 1237141279248703489 %}

Most content types found in the Gopherverse are supported. Those includes:

- plain text
- menus
- input fields
- images
- audio
- video
- HTML (sandboxed, no external requests!)
- PDF
- Binary downloads

For usability, it is designed with:

- Responsive text scaling for phone, tablet, and desktop.
- Themes with dark/light modes.
- Delegating basic controls to the web browser (back/forward, bookmarks, reload, etc)

This client is also a proof of concept of two new experimental transport protocols that require *no changes to the Gopher protocol*.

- **Gopher over HTTP (GoH)** - Lightweight proxy tunnelling of Gopher data over HTTP request/response.
- **Gopher over TLS (GoT)** - Encrypted Gopher connections. Uses SNI to support virtual hosting by Gopher servers, and ALPN for forward compatibility.

## About Gopher

The Gopher protocol dates back to 1991 when the Internet was mostly used to publish text based content. A competing service, the World Wide Web with its HTML and HTTP standards, soon took over this role and has kept growing ever since. Nevertheless, a small but passionate community keeps the Gopherverse alive.

## Why?

Retro-computing fans continue to publish hundreds of ~~blogs~~ phlogs and other content on the Gopher network. Some see it as a fun way to experiment with network programming and server administration. To others Gopher is an escape from the ails of complex, modern websites. Gopher is free from commercialism, advertising, and tracking. The Gopher protocol is *so simple* that it does not support those *features*. Gaufre aims to make the Gopherverse accessible to a new generation of burrowers.

{% youtube lUBhOgK5zQI %}

*Video: The Web Is Broken Beyond Repair. The Alternative? GOPHER! (by DistroTube on Youtube)*

## Experimental Protocols

With Gaufre, two new protocol designs are introduced to help bring Gopher into the modern Web Platform, without disturbing its charming simplicity.

![](./gopher-diagram.png)

The Gaufre client runs client-side in your browser, where it is sandboxed for security reasons and can not access Gopher's raw TCP/IP sockets on port 70. The solution: *Gopher over HTTP (GoH)*. Gaufre makes an HTTP request to a very lightweight Gopher over HTTP proxy, which relays the request to the intended Gopher server. The Gopher server's response is returned as raw Gopher data through an HTTP response to Gaufre. Because a GoH proxy does not need to parse Gopher content, it is very CPU and RAM efficient compared to traditional Gopher to HTML proxies.

Another limitation of the Gopher protocol is its lack of virtual hosting, meaning multiple domain names on the same IP address. This stems from an era where one server typically had one domain name or could receive multiple IPv4 addresses. Today IPv4 addresses are expensve and a server can have any number of domain names pointed at it, for example a CDN edge serving *lots* of different websites. The HTTP/1.1 standard solved this problem by introducing a `Host: example.com` header which allowed browsers to tell a web server *which domain* they were accessing with a given URL path and querystring. Since Gopher protocol has no headers, there is no clear way to extend the protocol and the community has adopted workarounds like using subdirectories instead of domain names.

Then there is the lack of encryption; normal in 1991 but unacceptable in today's world. This is a curiously similar situation to DNS, an even older protocol that has no built-in encryption. Recent innovations for DNS include [DNS over HTTPS]() and [DNS over TLS](). The latter is what inspired *Gopher over TLS (GoT)*: Accepting both plaintext and TLS-encrypted Gopher connections on the same port 70/TCP.

Using the TLS SNI extension, a GoT client provides the server's domain name to the GoT server. This solves our problems and provides two key features: Virtual hosting and encrypted connections!

### Gopher over HTTP (GoH)

The *GoH protocol* makes it possible for any device or platform with an HTTP client to access raw Gopher traffic through a very simple proxy. It is not limited only to Gopher browsers like Gaufre. This is different from existing Gopher to HTML proxies which transform Gopher content into human-friendly but machine-unfriendly rich markup.

The Commons Host CDN project offers a public GoH proxy service, set as default in Gaufre. You can also run your own GoH proxy, on your own machine or network, for absolute performance and total control. With Node.js installed, run this to start a lightweight GoH proxy server:

```shell
$ npx goh
```

GoH specification and implementation: https://gitlab.com/commonshost/goh

### Gopher over TLS (GoT)

The *GoT protocol* allows hosting of multiple domains on a single IP address, something Gopher cannot do otherwise. I have used this ability to create a Gopher CDN and hosting service, available at gopher://commons.host. It should even be possible, though I have not yet attempted, to place a GoT socket forwarder on port 70 which routes traffic based on the SNI servername to any (unmodified) Gopher servers running on different ports. A similar design, though without SNI and ALPN, was [proposed last year by Solène using sslh_fork](https://dataswamp.org/~solene/2019-03-07-gopher-server-tls.html).

GoT is supported by the GoH proxy (acting as a GoT client). The first Gopher server to support GoH is [gopher://commons.host](gopher://commons.host) and any static sites hosted on its CDN.

GoT specification and implementation: https://gitlab.com/commonshost/goth

## Happy Burrowing!

Please give Gaufre a try and enjoy exploring the Gopherverse.

Launch Gaufre: https://gopher.commons.host

*Cover photo: Gophers are known for their extensive tunneling activities. Their underground networks resemble the honeycomb-like pattern of waffles. Gaufre is the French word for waffle. Credit: [Airwolfhound](https://www.flickr.com/photos/24874528@N04/47336767931)*
