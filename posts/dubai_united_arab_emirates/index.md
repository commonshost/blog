---
date: 2018-08-10
title: Dubai, United Arab Emirates 🇦🇪
published: false
description: First CDN edge PoP deployment in MENA.
tags: cdn, edge, pop
---

Dubai, famous for its rapid growth and development, is an auspicious location for the first point-of-presence (PoP) of the [Commons Host CDN](https://commons.host) in the Middle East and North Africa (MENA).

![Dubai Internet City on Google Maps](./satellite_map.jpg)
*Photo: Google Maps screenshot marking the PoP location near Dubai's famous Palm Islands.*

This PoP is hosted within the Dubai Internet City free economic zone. The deployment is made possible thanks to the generous patronage of [Tien Nguyen](https://twitter.com/viettienn) who recently also funded the [Commons Host PoP in Can Tho, Vietnam](https://dev.to/commonshost/cn-th-vietnam--359j).

![Server plugged in at DIC](./server_rack.jpg)
*Photo: The PoP (left) was up and running within minutes of plugging in the power and network cables.*

The deployed hardware platform is the [Little Lamb Mk I](https://dev.to/commonshost/little-lamb-mk-i-5gf3), an efficient, ARM based, micro-server equipped with SSD storage.

## A Single Grain of Sand in the Desert

This first Commons Host PoP is just a tiny step.

Hundreds of millions of people live in the MENA region. Frustration with poorly performing Internet access is a huge problem, as evidenced by movements like the [Internet Revolution Egypt (IRE)](https://en.wikipedia.org/wiki/Internet_Revolution_Egypt).

To serve this many Internet users requires a lot of physical infrastructure. While the necessary large scale investments are happening, there is much that can be done by individuals and SMEs.

Commons Host is more than just clever software running in a few cloud data centres. To reach *lots of people* around the world requires *lots of hardware* around the world.

Deploying local CDN edge PoPs can help cope with congested backbones and long distance round trip times.

If you are a technology entrepreneur or engineer in the region, please reach out and help build a faster Internet for your customers.
