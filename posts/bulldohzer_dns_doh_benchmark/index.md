---
date: 2019-02-03
title: Bulldohzer 🚜 DNS & DoH performance testing
tags: showdev, opensource, dns, webperf
---

![Cover](./4870139498_5f0ff1af7e_b.jpg)

A few months ago [Commons Host](https://commons.host) built and launched its DNS over HTTPS service [in just 10 days](https://dev.to/commonshost/how-we-built-a-doh-cdn-with-20-global-edge-servers-in-10-days-1man). The service has proved reliable and performant, with users enjoying secure and private DNS service.

I have no view on the number of Commons Host DoH users. No request metrics are currently recorded by the edge servers. But other experimental DoH services, like [PowerDNS](https://powerdns.org), describe growing popularity.

One challenge users face is finding the best DoH or DNS service. Public performance metrics and benchmarks are often little more than thinly veiled content marketing. Even when the tests and numbers are legitimate. This is because performance at a test server in a big datacentre or at an Internet Exchange is not the same as on your own device on your own network.

## Introducing: Bulldohzer 🚜

[Bulldohzer](https://www.npmjs.com/package/bulldohzer) is an easy to use DNS and DoH performance test. You can run Bulldohzer yourself to find the best resolver *for you*.

Bulldohzer does not require any installation. Test runs take just a few seconds. Reports are designed to offer a lot of detail yet be easy to understand at a glance. Output of raw JSON data is also supported.

```
$ npx bulldohzer
```

*Note: The `npx` command is provided by Node.js which is the only dependency. You will need Node.js v11.4.0 or later.*

![Screenshot](./bulldohzer.png)

## If you can not measure it, you can not improve it.

Traditional DNS is heavily optimised due to decades of widespread use. Unfortunately it is susceptible to tampering and monitoring. DoH is a new and secure DNS protocol. DoH transports DNS over long-lived HTTP/2 connections. Because DoH is so new, some implementations are not yet optimised nor widely deployed.

Please try out Bulldohzer and share your results with DoH providers.

*Cover photo by [khaosproductions](https://www.flickr.com/photos/khaosproductions/4870139498/)*
