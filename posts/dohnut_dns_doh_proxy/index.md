---
date: 2019-02-20
title: Dohnut 🍩 DNS to DoH proxy
tags: showdev, opensource, dns
---

*TL;DR [Dohnut](https://help.commons.host/dohnut/) easily upgrades all your network clients by proxying plaintext DNS to encrypted DoH.*

The [Commons Host](https://commons.host) CDN project [recently launched](https://dev.to/commonshost/how-we-built-a-doh-cdn-with-20-global-edge-servers-in-10-days-1man) a public DNS-over-HTTPS (DoH) service. DoH now operates across all 30+ edge servers of the Commons Host network, offering low latency in many locations worldwide. Uniquely the Commons Host network is grown by contributors who [own and host low cost micro-servers](https://dev.to/commonshost/little-lamb-mk-i-5gf3) using consumer-grade Internet connections at their homes or offices.

The DoH Internet standard, [RFC8484](https://tools.ietf.org/html/rfc8484), promises improved privacy and security for DNS. DoH encrypts all queries, protecting users against snooping or DNS response tampering by ISPs and rogue Wi-Fi routers.

Upgrading all of your network clients from plaintext DNS to encrypted DoH is not trivial. There is currently no operating system or router/hardware support. The only browser supporting DoH today is Firefox.

This is why a DNS to DoH proxy is needed.

## Introducing: Dohnut 🍩

Dohnut acts as a local DNS server, either for one machine or for an entire local network. It proxies all DNS queries to remote DoH services inside encrypted, long-lived HTTP/2 connections.

![Dohnut overview diagram](./dohnut-overview.png)

## Easy to Deploy

[Deployment guides](https://help.commons.host/dohnut/) are currently available for Raspbian, Docker, Linux/systemd, and macOS/launchd.

A desktop client and a web dashboard are in the works!

## Lightweight

Dohnut is built with Node.js to be cross-platform and fast. Running on just a $35 Raspberry Pi computer, Dohnut can easily handle a typical home or SME network with dozens of DNS clients.

{% twitter 1092304430739771392 %}

Dohnut is also a great companion to the popular DNS ad-blocker Pi-hole. Dohnut acts as the Custom DNS Upstream server to Pi-hole. Pi-hole, as the DNS server to DNS clients on the network, does the ad-blocking, monitoring, and provides a local, low latency DNS cache.

{% twitter 1098130028787724289 %}

## Auto-Optimising DNS Latency

Multiple DoH services can be used by Dohnut at once. Dohnut load balances between DoH services using two configurable strategies:

- **Best performance**: Always send DNS queries to the fastest DoH resolver. Continuously monitors the round-trip-time latency to each DoH resolver using HTTP/2 PING frames. Set and forget; this mode automatically discovers when one of the DoH resolvers improves their latency to your network (e.g. deploying a new server near you).

- **Best privacy**: Uniformly distributes DNS queries across all enabled DoH resolvers. This shards DNS queries so that a single DoH resolver only sees a slice of the total traffic.

*Tip: Use [Bulldohzer](https://dev.to/commonshost/bulldohzer--dns--doh-performance-testing-50fm) to measure lookup latency from your location to multiple DNS and DoH resolvers.*

## Experimental: Active Tracking Countermeasures

Privacy policies of public DNS resolvers vary. But there is always the unavoidable fact that resolvers must see your DNS queries. This is an inherent privacy risk when using a [DNS-over-Cloud provider](https://blog.powerdns.com/2019/02/07/the-big-dns-privacy-debate-at-fosdem/).

To deter tracking by DoC providers, Dohnut can spoof DNS queries. It does this at random times and using a realistic sampling of popular real-world domains. This makes it very hard for any DoC provider to tell, if they wanted to, which queries are really yours and which are just spoofed noise.

This does introduce additional traffic and load on public DNS services. This is intended as a privacy experiment.

Another concern with DoH is the increased surface for tracking at the HTTP layer. By muxing queries from multiple clients into a single DoH connection, Dohnut acts as an passive privacy mechanism. Dohnut can also randomise the HTTP `User-Agent` header based on real world browser usage data.

## Feedback

Feedback on these ideas and their implementation is greatly appreciated. ❤️ Blog comments, GitHub Issues, Twitter, etc.

*Cover photo by [Ferry Sitompul](https://www.flickr.com/photos/65991505@N08/8222939536/)*
