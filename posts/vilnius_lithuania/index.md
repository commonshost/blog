---
date: 2018-07-20
title: Vilnius, Lithuania 🇱🇹
published: true
description: First CDN edge PoP deployment in Europe.
tags: cdn, edge, pop
---

The Commons Host CDN has placed its first footprint in Europe with a point-of-presence (PoP) in Vilnius, Lithuania. This takes the network beyond its South East Asian roots.

![Vilnius city view](./vilnius_city.jpg)

Photo: [Sergei Gussev (Flickr)](https://www.flickr.com/photos/sergeigussev/37018865605)

The PoP, a [Little Lamb Mk I]() micro-server, is sponsored and hosted by [Zygis](http://zx23.net), an early days supporter of the Commons Host project. His expert advice over the past months has been very encouraging and significantly accelerated the rollout.

![Zygis in Vilnius wearing Commons Host swag tshirt](./zygis_selfie.jpg)

Photo: [Zygis](http://zx23.net) showing off sheep swag in Vilnius (selfie)

## Automation with Ansible

The main challenge with this deployment was the physical inaccessibility of the server. The initial PoPs in Singapore and Kuala Lumpur were deployed by hand: visiting on-site to install and configure the system. However it would not have been cost effictive to travel from Singapore to Lithuania to deploy a single server.

This was a job for Ansible, a Configuration Management (CM) automation tool. Over the past two months [Kenny Shen](https://machinesung.com) and I collaborated on the complete automation of Commons Host PoP deployments.

![Kenny and his custom keyboards](./kenny_keyboards.jpg)

Photo: [Kenny Shen](https://machinesung.com) operating next-level hacker keyboard

We now have the ability to set up, secure, and monitor the servers remotely. All code is available in the [commonshost/ansible](https://gitlab.com/commonshost/ansible) GitLab repository. Contributions welcome!

![Code contributors graph](./contributors_graph.png)

## More to Come

This newly created tooling will be the foundation for many more deployments, both physical machines as well as cloud servers.

The Vilnius PoP serves currently all Commons Host traffic for Europe, as directed by the Geo DNS load balancer. This is really not as much as it sounds like, yet.

Over the coming months more effort will be made to promote the service to developers. To support the growing demand, additional PoPs will be deployed. As more servers are added to the network, the global traffic load will be better dispersed and optimised for low latency. Exciting times.
