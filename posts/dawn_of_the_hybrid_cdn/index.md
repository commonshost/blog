---
date: 2018-04-29
title: Dawn of the Hybrid CDN
published: true
description: This presentation is an overview of what makes Commons Host uniquely fit to achieve the huge goal of becoming the world's largest CDN.
tags: cdn
---

I performed this talk at the [Web Performance Singapore](https://www.meetup.com/Singapore-JS/events/249730512/) meetup. This new meetup is organised by yours truly and its first session hosted by Cloudflare Singapore office. It was great to get Little Lamb into the hands of over 50 attendees.

As a result several generous sponsors so far funded half a dozen servers to be deployed in Vietnam, Malaysia, and Singapore. I also received a donation of several cloud VPS instances.

Amazing community response. Thanks everyone! ❤️ Expect more Commons Host CDN PoPs to go live in the coming weeks and months.

[![Presentation recording](./presentation_recording.jpg)](https://www.youtube.com/watch?v=MXQmrZPH1vg)

[https://www.youtube.com/watch?v=MXQmrZPH1vg](https://www.youtube.com/watch?v=MXQmrZPH1vg)

Thanks to [Engineers.SG](https://engineers.sg) volunteers for recording.
