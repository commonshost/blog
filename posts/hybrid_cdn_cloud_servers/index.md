---
date: 2018-09-28
title: 8 new CDN edge locations
published: true
description: Adding 8 cloud servers to grow the Commons Host hybrid CDN.
tags: showdev, opensource, startup, webperf
---

The [Commons Host](https://commons.host) content delivery network just grew by 8 locations.

### 🌎 America

- 🇺🇸 Atlanta, Georgia, USA
- 🇺🇸 Dallas, Texas, USA
- 🇺🇸 Newark, New Jersey, USA
- 🇺🇸 San Jose, California, USA

### 🌏 Asia

- 🇸🇬 Singapore
- 🇯🇵 Tokyo, Japan

### 🌍 Europe

- 🇩🇪 Frankfurt, Germany
- 🇬🇧 London, United Kingdom

## Supported by UIlicious, powered by Linode

This big bump in network reach is thanks to a generous in-kind contribution by [UIlicious](https://uilicious.com) cofounder Eugene Cheah ([@picocreator](https://twitter.com/picocreator)). UIlicious is a modern web development tool to automate user journey testing.

Eugene offered his support at the very first announcement of the Commons Host project. After fully automating deployment, and several re-designs of the GeoDNS-based global server load balancing (GSLB), these edge nodes were seamlessly added to the Commons Host network.

The new servers are virtual machines. They are located in all datacentres operated by cloud hosting company [Linode](https://www.linode.com). Top-tier datacentres are well connected to Internet exchanges where many ISPs and carriers meet, offering afforable connectivity and scalable performance. High bandwidth and low latency. Perfect, right?

[![UIlicious logo](./uilicious_logo.png)](https://uilicious.com)
[![Linode logo](./linode_logo.png)](https://www.linode.com)

## First World Problems

Therein lies the first world problem. These datacentres are located in highly developed countries generating 37.8% of the global economy (2018 nominal GDP, IMF).

Bandwidth consumption in these markets is high. *Very high.* Fastly reports [peak traffic of 5.2 Tb/s](https://www.fastly.com/blog/altitude-sf-2018-recap) during the 2018 SuperBowl. They claim typical sustained rates over 3 Tb/s. Other big, mostly American, CDNs claim similar figures or more.

Even if cloud servers like Linode's could reach sustained bandwidth of the claimed 1 Gbps, [which they really don't](https://www.vpsbenchmarks.com/trials/linode_performance_trial_10Jun2018/network_transfers), it would take thousands to handle peak loads.

In reality it would also take dozens to hundreds of locations to directly peer (aka physically connect via fibre cables) with as many ISPs as possible. Cloud vendors simply do not offer this service today. Still too often negotiations between CDNs, carriers, and ISPs take place at [peering conferences](https://www.peeringforum.com), an expensive and slow process only afforded the largest companies.

This has led to enormous concentration of the Internet between a handful of giant tech companies. The consequences are [being dealt with](https://ssd.eff.org) across the world, as people consider the inherent threats to their privacy, freedom of speech, and democracy.

## Third World Solutions

Centralisation of another kind lies at the heart of the [lagging Internet hosting infrastructure](http://www.speedtest.net/global-index) in developing economies. The cause is often political and regulatory, caused by limited market competition, the result of lingering telecom monopolies.

Created in South East Asia to solve regional problems, the approach Commons Host takes is to side-step the centralised bottlenecks. [Small, affordable ARM-based micro-servers](https://sg.carousell.com/p/little-lamb-mk-i-commons-host-cdn-pop-edge-server-166149743/) are deployed at business or consumer fibre connections around the world. Commodity fibre network speeds, at least within the same ISP or state/country, are often better than cheap VPSes like Linode's. And the variety of edge locations is practically any home or office in the world.

This means latency is lower while bandwidth-per-server does not need to be as high as centralised CDNs. And peering is a natural consequence of being housed on an actual ISP, rather than bureaucratic negotiations at internet exchanges.

## Hybrid CDN: Best of Both Worlds?

The unique strength of Commons Host is the ability to combine datacentres across Europe, America, and other regions, with a vast network of micro-servers deployed worldwide.

Designed for this scale, everything in deployment and management must be fully automated and made as easy to use as possible.

Based on these principles, Commons Host offers a great hosting platform for truly global static web hosting, with more to come very soon.
