---
date: 2018-12-03
title: Amaravati, India 🇮🇳
published: false
description: A milestone for the decentralised collaboration.
tags: opensource, cdn, india
---

A new edge server has joined [Commons Host](https://commons.host) in [Amaravati](https://en.wikipedia.org/wiki/Amaravati), [Andhra Pradesh Capital Region](https://en.wikipedia.org/wiki/Andhra_Pradesh_Capital_Region), India. The hardware is contributed by [Sai Ram Kunala](https://sairam.xyz) ([@sairam](https://twitter.com/sairam)) and deployed on commodity fibre-to-the-home (FTTH), which is [available in most Indian cities](https://www.google.com/maps/d/viewer?mid=z6U1ilWPGIps.kbUaXWBZjvGg&amp;hl=en_US). For users in India this brings higher bandwidth and lower latency to the Commons Host static site CDN and DNS-over-HTTPS (DoH) service.

![Panorama of Vijayawada](./Vijayawada_landscape.jpg)

*Photo: Panorama of Vijayawada, the most populous city within the Andhra Pradesh Capital Region (Source: [Yedla70](https://en.wikipedia.org/wiki/Vijayawada#/media/File:Vijayawada_landscape.jpg))*

## >2x Better DNS Latency in India

The performance improvement is especially noticeable with DNS-over-HTTPS vs unencrypted DNS.

Commons Host uses Geo DNS based routing to ensure traffic from users in India stays within India, resulting in optimal round-trip-times. Other providers, using Anycast IP addresses for their public DNS resolvers, appear to suffer from traffic routing to far-away Singapore.

These are latency measurements by [Site24x7](https://www.site24x7.com/ping-test.html), a third-party ping service present in Chennai, India.

| Provider | Address | Routed To | Latency | Distance |
|-|-|-|-|-|-|
| Commons Host DoH | `commons.host` | Vijayawada | [14 ms](https://www.site24x7.com/public/t/results-1543807238932.html) | 400 km |
| Google DNS | `8.8.8.8` | Singapore | [34 ms](https://www.site24x7.com/public/t/results-1543809332730.html) | 2900 km |
| Cloudflare DNS | `1.1.1.1` | Singapore | [34 ms](https://www.site24x7.com/public/t/results-1543816016949.html) | 2900 km |
| Quad9 DNS | `9.9.9.9` | Singapore | [36 ms](https://www.site24x7.com/public/t/results-1543816937859.html) | 2900 km |

Note: These numbers show the baseline network latency. Cache hit ratios also affect actual DNS lookup times and vary by provider.

## Freedom to Contribute

Many thanks to Sai Ram for his remarkable initiative. The deployment is the first with 100% independently sourced hardware. It is a symbolic milestone. Sai Ram simply found the Commons Host project website, watched the videos and read the blog, and purchased his own components based on the [Little Lamb Mk I](https://dev.to/commonshost/little-lamb-mk-i-5gf3) open hardware specs. Then he reached out to help grow the network.

This bypassed a bottleneck: centralised assembly and shipping of edge servers.

Taking ownership of the hardware acquisition means contributors like Sai Ram require neither permission nor resources from any centralised leadership. Simply deploy an edge server and make it available for activation on the Commons Host CDN.

## Fundamentally Decentralised

Every country is a unique environment with different challenges and opportunities. Deploying servers around the world is an overwhelming endeavour when viewed from a centralised perspective. Even well funded and expertly staffed teams at large companies struggle to deploy hardware in many places that need it most. That means much of Asia, Africa, LatAm, and MENA is underserved.

The Commons Host approach is different.

1. **Infrastructure**: Physical ownership, funding, and hosting of edge servers by worldwide contributors. No single company owns the hardware. This strategy absorbs differences in local infrastructure, economies, laws & regulations, logistics, language barriers, etc.

1. **Tools**: All code for the service is developed as free & open source software (FOSS) by independent collaborators. Anyone can review it and contribute patches.

1. **Management**: Secure, remote configuration is handled through automation tools. Since local unauthorised hardware access is assumed, the software is designed for a more hostile environment than a typical data centre.

Decentralised physical ownership directs more human energy at solving the hard problems of infrastructure and logistics at a hyper-local level. Yet by pooling their effort, a better global service is provided to all.

## Network Effect

In theory any of the edge server owners, alone or in partnership, can fork the code and manage their own network using the exact same FOSS tools. The project leadership is kept from becoming complacent by the threat of abandonment.

In practice, however, there is a shared incentive to maintain and grow a combined, global service. A splintered network would not offer the geographical reach of the cohesive Commons Host service.

What keeps Commons Host together is [Metcalfe's law](https://en.wikipedia.org/wiki/Metcalfe's_law). Roughly speaking, the value of a network is the square of its size. The more nodes in the network, the more advantageous it is for new nodes to join the same network.

Similar mechanisms work for other collaborative projects like the Linux Kernel or Wikipedia. They started as a small and scrappy group of idealists but eventually bested large incumbents.
