## Other Uses for Server Push

There is more to push than optimising browser resource loading.

Other use cases are being proposed. For example the use of server push to deliver Hypermedia API query response records. Rather than returning entire inlined collections of nested JSON objects, the use of references to individually pushed records could be combined with a Cache Digest.

1. Client makes a query.

```
Request: /people?name=Jane
```

1. Service delivers individual records via Server Push, immediately followed by a response to the original request uses record references.

```
Push: { id: 1, name: 'Jane Doe' }
Push: { id: 2, name: 'Jane Mo' }
Push: { id: 3, name: 'Janet Doe' }
Response: [{ id: 1 }, { id: 2 }, { id: 3 }]
```

Thanks to HTTP/2 header compression this requires barely any more data transfer as an inlined JSON result.

1. Client makes requests which the browser answers from its cache, eliminating any network round trips.

```
Request: /person?id=1 -> Cache hit!
Request: /person?id=2 -> Cache hit!
Request: /person?id=3 -> Cache hit!
```

1. Some time later, the client makes a request with overlapping result set.

```
Request: /people?name=Janet
```

1. The server knows the client already received the individual record.

```
Response: [{id: 3}]
```

## 

Supporting Cache Digests with the Diary would be quite trivial. If browser vendors and other web developers indicate interest, I will gladly assist.

## How Do I Use Server Push Diaries?

They require no additional effort and are enabled automatically when any resource is pushed.

To use server push, generate a Server Push Manifest for your site. Simply run:

```
npx @commonshost/manifest generate
```

This traces all dependencies in HTML, CSS, and JS resources and saves the necessary server push instructions to `serverpush.json`.

Deploy your site with the usual command which will auto-detect the manifest file.

```
npx @commonshost/cli deploy
```

Your site should now be faster using Server Push, and without the over push problem.

## Why Does Server Push Matter?

Because it can make the web faster for users and easier for developers.

Server to client network latency is one of the biggest performance bottlenecks. There are two ways to overcome latency:

1. Shorten the network distance between server and client.
1. Reduce the number of serial round trips.

The first solution lies in the nature of CDNs: Servers are spread around the world. At the constant speed of light, data is transferred over a shorter average distance in less time.

The second solution is to join multiple requests together. Plenty of tools, like WebPack or RollUp, exist to concatenate files at build-time. This worked great at reducing HTTP requests. But there are trade-offs.

1. Development, deployment, and debugging of front-end assets is made more complicated by additional tools.
1. Caching is less efficient since all concatenated files are invalidated when any part of the bundle is modified.
1. Overlapping code shared between different pages, such as libraries, must be delicately split into ever more complex sub-bundles.

Server Push in HTTP/2 offers protocol-level resource concatenation. Sounds good in theory, but in practice it was soon discovered that problems exist. Over push is a 
